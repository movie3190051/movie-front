import { configureStore } from "@reduxjs/toolkit";
import couterReducer from "./slice/counterSlice";
import movieReducer from "./slice/movieSlice";
export const store = configureStore({
  reducer: {
    counter: couterReducer,
    movie: movieReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
