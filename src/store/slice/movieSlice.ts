import { createSlice } from "@reduxjs/toolkit";
import Movie from "../../types/movie";

interface MovieState {
  movies: Movie[];
  selectedMovie: Movie | null;
}
const initialState: MovieState = {
  movies: [
    {
      id: 1,
      name: "4Kings",
    },
  ],
  selectedMovie: null,
};
export const movieSlice = createSlice({
  name: "movie",
  initialState,
  reducers: {},
  extraReducers(builder) {},
});

export default movieSlice.reducer;
