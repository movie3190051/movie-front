import { createBrowserRouter } from "react-router-dom";
import Home from "../Home";
import MovieDetail from "../views/movie/MovieDetail";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/movie/:movieId",
    element: <MovieDetail />,
  },
]);

export default router;
